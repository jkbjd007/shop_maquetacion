import 'package:flutter/cupertino.dart';
import 'dart:ui';

class size{
  static convertLongestSide(BuildContext context,double n){
    double i = (n)/683;
    return i*MediaQuery.of(context).size.longestSide;
  }
  static convertHeight(BuildContext context,double n){
    double i = (n)/683;
    return i*MediaQuery.of(context).size.height;
  }
  static convertWidth(BuildContext context,double n){
    double i = (n)/411;
    return i*MediaQuery.of(context).size.width;
  }
}


