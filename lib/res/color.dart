import 'package:flutter/cupertino.dart';

Color appColor = Color(0xff94cc00);
Color buttonColor = Color(0xffff8f00);
Color hintsColor = Color(0xffcbcbcb);
Color IconsColor = Color(0xff185729);
Color timeSlotColor = Color(0xff5fe5bc);
Color whiteColor = Color(0xffffffff);
Color errorColor = Color(0xffff0000);
Color logoColor = Color(0xff097984);
Color BlackFont = Color(0xff000000);
Color whiteFont = Color(0xffffffff);
