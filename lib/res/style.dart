import 'package:flutter/material.dart';
import 'package:shop_maquetacion/res/color.dart';
class styles{
  static TextStyle PoppinsMedium ({double fontSize = 17 , String fontFamily = "Poppins-Medium", Color  color}){
    return TextStyle(
    fontFamily: fontFamily,
    fontSize: fontSize,
color: color??Color(0xff919191)
);
}

  static TextStyle PoppinsSemiBold ({double fontSize = 19 , String fontFamily = "Poppins-SemiBold", Color  color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle PoppinsExtraBold({double fontSize = 17 , String fontFamily = "Poppins-ExtraBold", Color  color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }

  static TextStyle PoppinsBlack({double fontSize = 20 , String fontFamily = "Poppins-Black", Color  color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle PoppinsSemiBoldBold({double fontSize = 20 , String fontFamily = "Montserrat-SemiBold",Color color}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color??hintsColor,
        fontWeight: FontWeight.bold,

    );
  }
  static TextStyle PoppinsRegular({double fontSize = 18 , String fontFamily = "Poppins-Regular", Color color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle PoppinsBold({double fontSize = 9 , String fontFamily = "Poppins-Bold", Color color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle RobotoMedium({double fontSize = 10 , String fontFamily = "RobotoMedium", Color color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
}

