import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shop_maquetacion/UIDesign/card.dart';
import 'package:shop_maquetacion/UIDesign/forgotPassword.dart';
import 'package:shop_maquetacion/UIDesign/homePage.dart';
import 'package:shop_maquetacion/UIDesign/product.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomBottomAppBar.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomDrawer.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomTextField.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/repeatedWigets/radioButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/sizeConfig.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class Favorities extends StatefulWidget {
  @override
  _FavoritiesState createState() => _FavoritiesState();
}

class _FavoritiesState extends State<Favorities> {

  List fruid = [
    {
      "icon":"assets/icons/Platano.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Platano"
    },
    {
      "icon":"assets/icons/Arandano.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Arandano"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Manzana.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Manzana"
    }
  ];
  List fruid1 = [
    {
      "icon":"assets/icons/Platano.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Platano"
    },
    {
      "icon":"assets/icons/Arandano.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Arandano"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Manzana.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Manzana"
    },
    {
      "icon":"assets/icons/Platano.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Platano"
    },
    {
      "icon":"assets/icons/Arandano.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Arandano"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Manzana.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Manzana"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    }
  ];
  bool detailBody = false;
  GlobalKey<ScaffoldState> scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldkey,
      drawer: openDrawer(),
      body:  body() ,
      bottomNavigationBar: CustomBottomBar(
        select: 4,
        scaffoldkey: scaffoldkey,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked, //specify the location of the FAB
      floatingActionButton: Container(
        child: GestureDetector(
          onTap: () {
            print("floatingActionButton");
            Navigator.push(context, PageTransition(child: card(),type: PageTransitionType.leftToRight));
          },
          child: Container(
            padding: EdgeInsets.all(10),
            height: size.convertLongestSide(context, 80),
            width: size.convertLongestSide(context, 80),
            child: FloatingActionButton(
              backgroundColor: buttonColor,
              elevation: 25,
              //onPressed: _onFloatingPress,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset("assets/icons/cardCare.png")

                  ],
                ),
              ),
            ),
          ),
        ),
      ),

    );
  }
  openDrawer() {
    return Drawer(
      child: CustomDrawer(),
    );
  }


  body(){
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: size.convertLongestSide(context, 100),),
                  Container(
                    margin: MediaQuery.of(context).orientation == Orientation.landscape ? EdgeInsets.symmetric(horizontal: size.convertWidth(context, 0)) : EdgeInsets.symmetric(horizontal: size.convertWidth(context, 14)),
                    child: StaggeredGridView.countBuilder(
                      padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      crossAxisCount: 3,
                      itemCount: fruid1.length,
                      itemBuilder: (BuildContext context, int index) =>
                          Container(
                            //width: size.convertLongestSide(context, 80),
                              margin: EdgeInsets.symmetric(vertical: 4,horizontal: 0),
                              padding: EdgeInsets.only(left: 8,right: 8,top: 8),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                  boxShadow: [ //background color of box
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.09),
                                      spreadRadius: 2,
                                      blurRadius: 5,
                                      offset: Offset(-1,-1),

                                    )
                                  ]
                              ),
                              child:  Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Image.asset(fruid1[index]["icon"]),
                                  SizedBox(height: size.convertLongestSide(context, 10),),
                                  Container(
                                    //color: Colors.green,
                                    width: size.convertLongestSide(context, 80),
                                    child: Row(children: <Widget>[
                                      Expanded(
                                        child: Container(child: Column(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.symmetric(horizontal: 4),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Container(
                                                      alignment: Alignment.centerLeft,
                                                      //color:Colors.red,
                                                      child: RichText(
                                                        maxLines: 1,
                                                        //textAlign: TextAlign.center,
                                                        text: TextSpan(
                                                            text: fruid1[index]["price"],
                                                            style: styles.PoppinsSemiBold(
                                                                fontSize: size.convertLongestSide(context, 8)
                                                            )
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                    child: RichText(
                                                      maxLines: 1,
                                                      textAlign: TextAlign.center,
                                                      text: TextSpan(
                                                          text: fruid1[index]["quantity"],
                                                          style: styles.PoppinsSemiBold(
                                                              fontSize: size.convertLongestSide(context, 8)
                                                          )
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.symmetric(horizontal: 4),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Container(
                                                      alignment: Alignment.centerLeft,
                                                      child: RichText(
                                                        maxLines: 2,
                                                        //textAlign: TextAlign.center,
                                                        text: TextSpan(
                                                            text: fruid1[index]["name"],
                                                            style: styles.PoppinsBlack(
                                                                fontSize: size.convertLongestSide(context, 9)
                                                            )
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(4),
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: buttonColor
                                        ),
                                        child: Image.asset("assets/icons/cardCareSmall.png"),
                                      )
                                    ],
                                    ),
                                  )
                                ],
                              )
                          ),
                      staggeredTileBuilder: (int index) =>
                      new StaggeredTile.fit(1),
                      mainAxisSpacing: 15,
                      crossAxisSpacing: 15,
                    ),
                  ),
                  SizedBox(height: size.convertLongestSide(context, 25),),
                ],),
            ),
          ),
          backButton(
            title: "Favoritos",
            istitle: true,
          )
        ],
      ),
    );
  }
}
