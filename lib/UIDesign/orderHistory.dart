import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shop_maquetacion/UIDesign/card.dart';
import 'package:shop_maquetacion/UIDesign/forgotPassword.dart';
import 'package:shop_maquetacion/UIDesign/homePage.dart';
import 'package:shop_maquetacion/UIDesign/product.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomBottomAppBar.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomDrawer.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomTextField.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/repeatedWigets/radioButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/sizeConfig.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class orderHistory extends StatefulWidget {
  @override
  _orderHistoryState createState() => _orderHistoryState();
}

class _orderHistoryState extends State<orderHistory> {
  List item = [
    {
      "name":"Orden #28",
      "icon":"assets/icons/Durazno.png",
      "price":100,
      "count":3,
    },
    {
      "name":"Orden #28",
      "icon":"assets/icons/Durazno.png",
      "price":100,
      "count":2,
    },
    {
      "name":"Orden #28",
      "icon":"assets/icons/Durazno.png",
      "price":100,
      "count":1,
    },
    {
      "name":"Orden #28",
      "icon":"assets/icons/Durazno.png",
      "price":100,
      "count":4,
    },
  ];
  GlobalKey<ScaffoldState> scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldkey,
      drawer: openDrawer(),
      body:  body() ,
      bottomNavigationBar: CustomBottomBar(
        select: 3,
        scaffoldkey: scaffoldkey,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked, //specify the location of the FAB
      floatingActionButton: Container(
        child: GestureDetector(
          onTap: () {
            print("floatingActionButton");
            Navigator.push(context, PageTransition(child: card(),type: PageTransitionType.leftToRight));
          },
          child: Container(
            padding: EdgeInsets.all(10),
            height: size.convertLongestSide(context, 80),
            width: size.convertLongestSide(context, 80),
            child: FloatingActionButton(
              backgroundColor: buttonColor,
              elevation: 25,
              //onPressed: _onFloatingPress,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset("assets/icons/cardCare.png")

                  ],
                ),
              ),
            ),
          ),
        ),
      ),

    );
  }
  openDrawer() {
    return Drawer(
      child: CustomDrawer(),
    );
  }


  body(){
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: size.convertLongestSide(context, 100),),
                  ListView.separated(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemBuilder: (BuildContext context, int index){
                      return Container(
                        padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 15),vertical: size.convertLongestSide(context, 10)),
                        margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 25)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black.withOpacity(0.05),
                                  blurRadius: 2,
                                  spreadRadius: 2,
                                  offset: Offset(3,3)
                              )
                            ]
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(child: Image.asset(item[index]["icon"]),),
                            SizedBox(width: size.convertWidth(context, 12),),
                            Expanded(
                              child: Container(child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(child: RichText(
                                        text: TextSpan(
                                            text: item[index]["name"],
                                            style: styles.PoppinsBlack(
                                                fontSize: size.convertLongestSide(context, 16)
                                            )
                                        ),
                                      ),),
                                      Container(child: RichText(
                                        text: TextSpan(
                                            text: "\$ ${item[index]["price"]*item[index]["count"]}",
                                            style: styles.PoppinsSemiBold(
                                                fontSize: size.convertLongestSide(context, 16)
                                            )
                                        ),
                                      ),),
                                    ],
                                  ),),
                                  SizedBox(height: size.convertLongestSide(context, 5),),
                                  Container(child: RichText(
                                    text: TextSpan(
                                      text: "12/08/ 2020",
                                      style: styles.PoppinsMedium(
                                        fontSize: size.convertLongestSide(context, 15)
                                      )
                                    ),
                                  ),)
                                ],
                              ),),
                            )
                          ],
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index){
                      return SizedBox(height: size.convertLongestSide(context, 17),);
                    },
                    itemCount: item.length,
                  ),
                  SizedBox(height: size.convertLongestSide(context, 25),),
                ],),
            ),
          ),
          backButton(
            title: "Historial",
            istitle: true,
          )
        ],
      ),
    );
  }
}
