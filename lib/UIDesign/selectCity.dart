import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shop_maquetacion/UIDesign/forgotPassword.dart';
import 'package:shop_maquetacion/UIDesign/homePage.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomTextField.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/repeatedWigets/radioButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
class selectCity extends StatefulWidget {
  @override
  _selectCityState createState() => _selectCityState();
}

class _selectCityState extends State<selectCity> {

  bool selectedCDMX = true;
  bool selectedQueretaro = false;
  bool selectedGuadalajara = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(body:selectCityBody());
  }

  selectCityBody(){
    return Container(
      child: Stack(
        children: <Widget>[
          Container(child:SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: size.convertLongestSide(context, 60),),
                Center(child: Container(
                  width: size.convertWidth(context, 176),
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        text: "¿En qué ciudad te encuentras?",
                        style: styles.PoppinsSemiBoldBold(
                            color: BlackFont,
                            fontSize: size.convertLongestSide(context, 19)
                        )
                    ),
                  ),
                )),

                SizedBox(height: size.convertLongestSide(context, 40),),
                Center(child: Image.asset("assets/icons/selectCity.png")),

                SizedBox(height: size.convertLongestSide(context, 40),),
                Center(
                  child: Container(
                    width: size.convertWidth(context, 260),
                    child: Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              selectedCDMX = true;
                              selectedGuadalajara=false;
                              selectedQueretaro = false;
                            });
                          },
                          child: radioButton(
                            selected: selectedCDMX,
                          ),
                        ),
                        SizedBox(width: size.convertWidth(context, 27),),
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              text: "CDMX",
                              style: styles.PoppinsSemiBoldBold(
                                  color: BlackFont,
                                  fontSize: size.convertLongestSide(context, 17)
                              )
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                SizedBox(height: size.convertLongestSide(context, 10),),
                Center(
                  child: Container(
                    width: size.convertWidth(context, 260),
                    child: Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              selectedCDMX = false;
                              selectedGuadalajara=false;
                              selectedQueretaro = true;
                            });
                          },
                          child: radioButton(
                            selected: selectedQueretaro,
                          ),
                        ),
                        SizedBox(width: size.convertWidth(context, 27),),
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              text: "Queretaro",
                              style: styles.PoppinsSemiBoldBold(
                                  color: BlackFont,
                                  fontSize: size.convertLongestSide(context, 17)
                              )
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                SizedBox(height: size.convertLongestSide(context, 10),),
                Center(
                  child: Container(
                    width: size.convertWidth(context, 260),
                    child: Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              selectedCDMX = false;
                              selectedGuadalajara=true;
                              selectedQueretaro = false;
                            });
                          },
                          child: radioButton(
                            selected: selectedGuadalajara,
                          ),
                        ),
                        SizedBox(width: size.convertWidth(context, 27),),
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              text: "Guadalajara",
                              style: styles.PoppinsSemiBoldBold(
                                  color: BlackFont,
                                  fontSize: size.convertLongestSide(context, 17)
                              )
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                SizedBox(height: size.convertLongestSide(context, 50),),
                Center(child: filledButton(
                  onTap: (){
                    Navigator.push(context, PageTransition(
                        child: homePage(),
                        type: PageTransitionType.leftToRight
                    ));
                  },
                  txt: "Siguiente",
                )),
                SizedBox(height: size.convertLongestSide(context, 10),),

              ],
            ),
          ),
          ),
        ],
      ),
    );
  }
}
