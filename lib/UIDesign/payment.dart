import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:shop_maquetacion/UIDesign/cardDetail.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomBottomAppBar.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/repeatedWigets/radioButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';

class payment extends StatefulWidget {
  @override
  _paymentState createState() => _paymentState();
}

class _paymentState extends State<payment> {
  bool isPriceExpand = false;
  List cards = [
    {
      "name": "Mastercard",
      "icon": "assets/icons/mastercard.png",
      "cardNo": "564 *** **** **784",
      "selected": true,
    },
    {
      "name": "Visa",
      "icon": "assets/icons/visa.png",
      "cardNo": "564 *** **** **784",
      "selected": false,
    },
  ];
  List cards1 = [
    {
      "name": "Efectivo",
      "icon": "assets/icons/dinero.png",
      "cardNo": "pagar en efectivo",
      "selected": false,
    },
    {
      "name": "Terminal Bancaria",
      "icon": "assets/icons/cal.png",
      "cardNo": "Llevamos terminal física",
      "selected": false,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(),
    );
  }

  body() {
    return Container(
      child: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(
                  top: size.convertLongestSide(context, 90),
                  bottom: isPriceExpand? size.convertLongestSide(context, 260) :size.convertLongestSide(context, 110)),
              child: Column(
                children: <Widget>[
                  body1(),
                ],
              ),
            ),
          ),
          backButton(
            backTap: (){
              Navigator.pop(context);
            },
            shadowColor: Colors.black.withOpacity(0.1),
            istitle: true,
            title: "Opciones de pago",
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: AnimatedContainer(
                height: isPriceExpand
                    ? size.convertLongestSide(context, 230)
                    : size.convertLongestSide(context, 90),
                duration: Duration(seconds: 1),
                padding: EdgeInsets.only(
                    top: size.convertLongestSide(context, 1),
                    bottom: size.convertLongestSide(context, 10)),
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.05),
                        blurRadius: 3,
                        spreadRadius: 3,
                      )
                    ],
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                    ),
                    color: Colors.white),
                child: Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(
                          left: size.convertWidth(context, 39),
                          right: size.convertWidth(context, 39),
                          top: size.convertLongestSide(context, 0),
                          bottom: isPriceExpand
                              ? size.convertLongestSide(context, 60)
                              : size.convertLongestSide(context, 0)),
                      child: SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    isPriceExpand = !isPriceExpand;
                                  });
                                },
                                child: Center(
                                  child: isPriceExpand
                                      ? Icon(
                                          Icons.keyboard_arrow_down,
                                          color: appColor,
                                        )
                                      : Icon(
                                          Icons.keyboard_arrow_up,
                                          color: appColor,
                                        ),
                                ),
                              ),
                            ),
                            isPriceExpand
                                ? Column(
                                    children: <Widget>[
                                      Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Container(
                                              child: RichText(
                                                text: TextSpan(
                                                    text: "Total parcial:",
                                                    style: styles.PoppinsMedium(
                                                        fontSize: size
                                                            .convertLongestSide(
                                                                context, 16),
                                                        color:
                                                            Color(0xff3a3d4e))),
                                              ),
                                            ),
                                            Container(
                                              child: RichText(
                                                text: TextSpan(
                                                    text: "\$ 300",
                                                    style: styles.PoppinsMedium(
                                                        fontSize: size
                                                            .convertLongestSide(
                                                                context, 16),
                                                        color:
                                                            Color(0xff3a3d4e))),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height:
                                            size.convertLongestSide(context, 5),
                                      ),
                                      Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Container(
                                              child: RichText(
                                                text: TextSpan(
                                                    text: "Costo de envio:",
                                                    style: styles.PoppinsMedium(
                                                        fontSize: size
                                                            .convertLongestSide(
                                                                context, 16),
                                                        color:
                                                            Color(0xff3a3d4e))),
                                              ),
                                            ),
                                            Container(
                                              child: RichText(
                                                text: TextSpan(
                                                    text: "\$ 50",
                                                    style: styles.PoppinsMedium(
                                                        fontSize: size
                                                            .convertLongestSide(
                                                                context, 16),
                                                        color:
                                                            Color(0xff3a3d4e))),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height:
                                            size.convertLongestSide(context, 5),
                                      ),
                                      Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Container(
                                              child: RichText(
                                                text: TextSpan(
                                                    text: "Comisión:",
                                                    style: styles.PoppinsMedium(
                                                        fontSize: size
                                                            .convertLongestSide(
                                                                context, 16),
                                                        color:
                                                            Color(0xff3a3d4e))),
                                              ),
                                            ),
                                            Container(
                                              child: RichText(
//                                textAlign: TextAlign.start,
                                                text: TextSpan(
                                                    text: "\$ 10",
                                                    style: styles.PoppinsMedium(
                                                        fontSize: size
                                                            .convertLongestSide(
                                                                context, 16),
                                                        color:
                                                            Color(0xff3a3d4e))),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: size.convertLongestSide(
                                            context, 15),
                                      ),
                                      Divider(
                                        color: appColor,
                                      ),
                                      Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Container(
                                              child: RichText(
                                                text: TextSpan(
                                                    text: "Total:",
                                                    style: styles.PoppinsSemiBold(
                                                        fontSize: size
                                                            .convertLongestSide(
                                                                context, 16),
                                                        color:
                                                            Color(0xff3a3d4e))),
                                              ),
                                            ),
                                            Container(
                                              child: RichText(
//                                textAlign: TextAlign.start,
                                                text: TextSpan(
                                                    text: "\$ 360",
                                                    style: styles.PoppinsSemiBold(
                                                        fontSize: size
                                                            .convertLongestSide(
                                                                context, 16),
                                                        color:
                                                            Color(0xff3a3d4e))),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  )
                                : Container(),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomCenter,
                      child: filledButton(
                        onTap: (){
                          Navigator.push(context, PageTransition(child: cardDetail(), type: PageTransitionType.leftToRight));
                        },
                        color1: IconsColor,
                        txt: "Pagar",
                      ),
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }

  body1() {
    return Container(
      margin: EdgeInsets.only(
          left: size.convertWidth(context, 42),
          right: size.convertWidth(context, 37)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: RichText(
              text: TextSpan(
                  text: "Pago con tarjeta",
                  style: styles.PoppinsSemiBold(
                      fontSize: size.convertLongestSide(context, 19))),
            ),
          ),
          Container(
            child: ListView.separated(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: size.convertWidth(context, 20),
                        vertical: size.convertLongestSide(context, 8)),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.05),
                            blurRadius: 6,
                            spreadRadius: 5,
                          )
                        ]),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Image.asset(cards[index]["icon"]),
                        Container(
                          child: Column(
//                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: RichText(
                                  text: TextSpan(
                                      text: cards[index]["name"],
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(
                                              context, 16),
                                          color: Color(0xff171101))),
                                ),
                              ),
                              Container(
                                child: RichText(
                                  text: TextSpan(
                                      text: cards[index]["cardNo"],
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(
                                              context, 12),
                                          color: Color(0xff757475))),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: radioButton(
                            selected: cards[index]["selected"],
                          ),
                        )
                      ],
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    height: size.convertLongestSide(context, 15),
                  );
                },
                itemCount: cards.length),
          ),
          SizedBox(
            height: size.convertLongestSide(context, 25),
          ),
          Container(
            child: RichText(
              text: TextSpan(
                  text: "Pago con tarjeta",
                  style: styles.PoppinsSemiBold(
                      fontSize: size.convertLongestSide(context, 19))),
            ),
          ),
          Container(
            child: ListView.separated(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: size.convertWidth(context, 20),
                        vertical: size.convertLongestSide(context, 8)),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.05),
                            blurRadius: 6,
                            spreadRadius: 5,
                          )
                        ]),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Image.asset(cards1[index]["icon"]),
                        Container(
                          child: Column(
//                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: RichText(
                                  text: TextSpan(
                                      text: cards1[index]["name"],
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(
                                              context, 16),
                                          color: Color(0xff171101))),
                                ),
                              ),
                              Container(
                                child: RichText(
                                  text: TextSpan(
                                      text: cards1[index]["cardNo"],
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(
                                              context, 12),
                                          color: Color(0xff757475))),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: radioButton(
                            selected: cards1[index]["selected"],
                          ),
                        )
                      ],
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    height: size.convertLongestSide(context, 15),
                  );
                },
                itemCount: cards.length),
          ),
          SizedBox(
            height: size.convertLongestSide(context, 7),
          ),
        ],
      ),
    );
  }
}
