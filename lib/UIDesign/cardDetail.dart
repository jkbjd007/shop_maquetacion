

import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:shop_maquetacion/UIDesign/thankYouPage.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomBottomAppBar.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomTextField.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/repeatedWigets/radioButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/string.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';

class cardDetail extends StatefulWidget {
  @override
  _cardDetailState createState() => _cardDetailState();
}

class _cardDetailState extends State<cardDetail> {
  FocusScopeNode currentFocus ;
  FocusNode _focusNode;
  String selectTitle;
  String selectyear;
  bool isPriceExpand = false;
  bool disableBottom = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(),
    );
  }
  body(){
    return Container(
      child:  Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 30)),
              margin: EdgeInsets.only(top: size.convertLongestSide(context, 90),
                  bottom: isPriceExpand? size.convertLongestSide(context, 260) :size.convertLongestSide(context, 110)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(child: Image.asset("assets/icons/card.png")),

                      Container(child: RichText(
                        text: TextSpan(
                          text: "Numero de tarjeta",
                          style: styles.PoppinsMedium(
                            fontSize: size.convertLongestSide(context, 12),
                            color: BlackFont
                          )
                        ),
                      ),),
                  SizedBox(height: size.convertLongestSide(context, 5),),
                  CustomTextField(
                    focusMode: true,
                    focusNode: _focusNode,
                    onSumbit: (val){
                      setState(() {
                        _focusNode.unfocus();
                      });
                    },
                    onEditingComplete: (){
                      setState(() {
                        disableBottom = false;
                      });
                      currentFocus.unfocus();
                    },
                    textInputType: TextInputType.number,
                    ontap: (){

                      setState(() {
                        disableBottom = true;
                      });
                      currentFocus = FocusScope.of(context);
                    },
                    onsaved: (val){
                      setState(() {
                        disableBottom = false;
                      });
                    },
                  ),
                  SizedBox(height: size.convertLongestSide(context, 10),),

                  Container(child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: size.convertWidth(context, 163),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(child: RichText(
                              text: TextSpan(
                                  text: "Mes",
                                  style: styles.PoppinsMedium(
                                      fontSize: size.convertLongestSide(context, 12),
                                      color: BlackFont
                                  )
                              ),
                            ),),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: appColor,
                                  width: 1
                                )
                              ),
                              child: DropdownButton(
                              underline: Container(),
                              style: styles.PoppinsMedium(
                                  fontSize: size.convertLongestSide(context, 16),
                                  color: Color(0xff838383)
                              ),
//              focusColor: Colors.blue,
                              isExpanded: true,
                              icon: Icon(Icons.keyboard_arrow_down,
                              color: appColor,
                              size: size.convertLongestSide(context, 30),),
                              hint: Text("enero",
                                style: styles.PoppinsMedium(
                                    fontSize: size.convertLongestSide(context, 15),
                                    color: Color(0xff838383)
                                ),),
                              // Not necessary for Option 1
                              value: selectTitle,
                              onChanged: (newValue) {
                                setState(() {
                                  selectTitle = newValue;
                                });
                              },


                              items: titleList.map((title) {
                                return DropdownMenuItem(
                                  child: new Text(title??"",
                                    style: styles.PoppinsMedium(
                                        fontSize: size.convertLongestSide(context, 16),
                                        color: Color(0xff838383)
                                    ),
                                  ),
                                  value: title,
                                );
                              }).toList(),
                            ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: size.convertWidth(context, 163),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(child: RichText(
                              text: TextSpan(
                                  text: "Año",
                                  style: styles.PoppinsMedium(
                                      fontSize: size.convertLongestSide(context, 12),
                                      color: BlackFont
                                  )
                              ),
                            ),),

                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                      color: appColor,
                                      width: 1
                                  )
                              ),
                              child: DropdownButton(
                                underline: Container(),
                                style: styles.PoppinsMedium(
                                    fontSize: size.convertLongestSide(context, 16),
                                    color: Color(0xff838383)
                                ),
//              focusColor: Colors.blue,
                                isExpanded: true,
                                icon: Icon(Icons.keyboard_arrow_down,
                                  color: appColor,
                                  size: size.convertLongestSide(context, 30),),
                                hint: Text(DateTime.now().year.toString(),
                                  style: styles.PoppinsMedium(
                                      fontSize: size.convertLongestSide(context, 15),
                                      color: Color(0xff838383)
                                  ),),
                                // Not necessary for Option 1
                                value: selectyear,
                                onChanged: (newValue) {
                                  setState(() {
                                    selectyear = newValue;
                                  });
                                },
                                items: year.map((title) {
                                  return DropdownMenuItem(
                                    child: new Text(title??"",
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(context, 16),
                                          color: Color(0xff838383)
                                      ),
                                    ),
                                    value: title,
                                  );
                                }).toList(),
                              ),
                            ),

                          ],
                        ),
                      ),
                    ],
                  ),),

                  SizedBox(height: size.convertLongestSide(context, 10),),
                  Container(

                    child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: size.convertWidth(context, 163),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(child: RichText(
                              text: TextSpan(
                                  text: "CCV",
                                  style: styles.PoppinsMedium(
                                      fontSize: size.convertLongestSide(context, 12),
                                      color: BlackFont
                                  )
                              ),
                            ),),
                            CustomTextField(
                              focusMode: true,
                              focusNode: _focusNode,
                              onSumbit: (val){
                                setState(() {
                                  _focusNode.unfocus();
                                });
                              },
                              onEditingComplete: (){
                                setState(() {
                                  disableBottom = false;
                                });
                                currentFocus.unfocus();
                              },
                              textInputType: TextInputType.number,
                              ontap: (){

                                setState(() {
                                  disableBottom = true;
                                });
                                currentFocus = FocusScope.of(context);
                              },
                              onsaved: (val){
                                setState(() {
                                  disableBottom = false;
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: size.convertWidth(context, 163),
                        child: Column(
                          children: <Widget>[
                            Container(height: 10,),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                radioButton(selected: false,),
                                SizedBox(width: 10,),
                                Container(child: RichText(
                                  text: TextSpan(
                                      text: "guardar tarjeta",
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(context, 12),
                                          color: BlackFont
                                      )
                                  ),
                                ),),



                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),),

                ],
              ),),
          ),
          backButton(
            backTap: (){
              Navigator.pop(context);
            },
            shadowColor: Colors.black.withOpacity(0.1),
            istitle: true,
            title: "Opciones de pago",

          ),
          disableBottom? Container(): Align(
            alignment: Alignment.bottomCenter,
            child: AnimatedContainer(
              height: isPriceExpand ? size.convertLongestSide(context, 230):size.convertLongestSide(context, 90),
              duration: Duration(seconds: 1),
              padding: EdgeInsets.only(top: size.convertLongestSide(context, 1),
              bottom: size.convertLongestSide(context, 10)),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.05),
                      blurRadius: 3,
                      spreadRadius: 3,

                    )
                  ],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                  color: Colors.white
              ),

              child: Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: size.convertWidth(context, 39),
                        right: size.convertWidth(context, 39),
                        top: size.convertLongestSide(context, 0),
                        bottom: isPriceExpand ? size.convertLongestSide(context, 60) :size.convertLongestSide(context, 0)
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: InkWell(
                              onTap: (){
                                setState(() {
                                  isPriceExpand = !isPriceExpand;
                                });
                              },
                              child: Center(
                                child:isPriceExpand ? Icon(Icons.keyboard_arrow_down , color: appColor,) : Icon(Icons.keyboard_arrow_up , color: appColor,),),
                            ),
                          ),
                          isPriceExpand ? Column(children: <Widget>[
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(child: RichText(
                                    text: TextSpan(
                                        text:"Total parcial:",
                                        style: styles.PoppinsMedium(
                                            fontSize: size.convertLongestSide(context, 16),
                                            color: Color(0xff3a3d4e)
                                        )
                                    ),
                                  ),),
                                  Container(child: RichText(
                                    text: TextSpan(
                                        text:"\$ 300",
                                        style: styles.PoppinsMedium(
                                            fontSize: size.convertLongestSide(context, 16),
                                            color: Color(0xff3a3d4e)
                                        )
                                    ),
                                  ),),
                                ],
                              ),
                            ),
                            SizedBox(height: size.convertLongestSide(context, 5),),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(child: RichText(
                                    text: TextSpan(
                                        text:"Costo de envio:",
                                        style: styles.PoppinsMedium(
                                            fontSize: size.convertLongestSide(context, 16),
                                            color: Color(0xff3a3d4e)
                                        )
                                    ),
                                  ),),
                                  Container(child: RichText(
                                    text: TextSpan(
                                        text:"\$ 50",
                                        style: styles.PoppinsMedium(
                                            fontSize: size.convertLongestSide(context, 16),
                                            color: Color(0xff3a3d4e)
                                        )
                                    ),
                                  ),),
                                ],
                              ),
                            ),
                            SizedBox(height: size.convertLongestSide(context, 5),),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(child: RichText(
                                    text: TextSpan(
                                        text:"Comisión:",
                                        style: styles.PoppinsMedium(
                                            fontSize: size.convertLongestSide(context, 16),
                                            color: Color(0xff3a3d4e)
                                        )
                                    ),
                                  ),),
                                  Container(
                                    child: RichText(
//                                textAlign: TextAlign.start,
                                      text: TextSpan(
                                          text:"\$ 10",
                                          style: styles.PoppinsMedium(
                                              fontSize: size.convertLongestSide(context, 16),
                                              color: Color(0xff3a3d4e)
                                          )
                                      ),
                                    ),),
                                ],
                              ),
                            ),
                            SizedBox(height: size.convertLongestSide(context, 15),),
                            Divider(color: appColor,),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(child: RichText(
                                    text: TextSpan(
                                        text:"Total:",
                                        style: styles.PoppinsSemiBold(
                                            fontSize: size.convertLongestSide(context, 16),
                                            color: Color(0xff3a3d4e)
                                        )
                                    ),
                                  ),),
                                  Container(
                                    child: RichText(
//                                textAlign: TextAlign.start,
                                      text: TextSpan(
                                          text:"\$ 360",
                                          style: styles.PoppinsSemiBold(
                                              fontSize: size.convertLongestSide(context, 16),
                                              color: Color(0xff3a3d4e)
                                          )
                                      ),
                                    ),),
                                ],
                              ),
                            ),
                          ],) : Container(),

                        ],
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.bottomCenter,
                    child: filledButton(
                      onTap: (){
                        Navigator.push(context, PageTransition(child: thankYouPage(),
                        type: PageTransitionType.leftToRight));
                      },
                      color1: IconsColor,
                      txt: "Pagar",
                    ),
                  ),

                ],
              )

            ),
          ),

        ],
      ),

    );
  }

}
