import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shop_maquetacion/UIDesign/deliveryAddress.dart';
import 'package:shop_maquetacion/UIDesign/product.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomBottomAppBar.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
class card extends StatefulWidget {
  @override
  _cardState createState() => _cardState();
}

class _cardState extends State<card> {
  List item = [
    {
      "name":"Durazno",
      "icon":"assets/icons/Durazno.png",
      "price":100,
      "count":3,
    },
    {
      "name":"Durazno",
      "icon":"assets/icons/Durazno.png",
      "price":100,
      "count":2,
    },
    {
      "name":"Durazno",
      "icon":"assets/icons/Durazno.png",
      "price":100,
      "count":1,
    },
    {
      "name":"Durazno",
      "icon":"assets/icons/Durazno.png",
      "price":100,
      "count":4,
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(),
    );
  }
  body(){
    return Container(
      child:  Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: size.convertLongestSide(context, 90),),
                  ListView.separated(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemBuilder: (BuildContext context, int index){
                      return GestureDetector(
                        onTap: (){
                          Navigator.push(context, PageTransition(child: product(),
                          type: PageTransitionType.leftToRight));
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 15),vertical: size.convertLongestSide(context, 10)),
                          margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 25)),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.05),
                                blurRadius: 2,
                                spreadRadius: 2,
                                offset: Offset(3,3)
                              )
                            ]
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(child: Image.asset(item[index]["icon"]),),
                              SizedBox(width: size.convertWidth(context, 12),),
                              Expanded(
                                child: Container(child: Column(
                                  children: <Widget>[
                                    Container(child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(child: RichText(
                                          text: TextSpan(
                                              text: item[index]["name"],
                                              style: styles.PoppinsBlack(
                                                  fontSize: size.convertLongestSide(context, 16)
                                              )
                                          ),
                                        ),),
                                        Container(child: RichText(
                                          text: TextSpan(
                                              text: "\$ ${item[index]["price"]*item[index]["count"]}",
                                              style: styles.PoppinsSemiBold(
                                                  fontSize: size.convertLongestSide(context, 16)
                                              )
                                          ),
                                        ),),
                                      ],
                                    ),),
                                    SizedBox(height: size.convertLongestSide(context, 15),),
                                    Container(

                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          GestureDetector(
                                            onTap: (){
                                              if(item[index]["count"]>1){
                                                setState(() {
                                                  item[index]["count"]--;
                                                });
                                              }
                                            },
                                            child: Container(
                                              width: size.convertLongestSide(context, 33),
                                              height: size.convertLongestSide(context, 33),
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(5),
                                                  color: appColor
                                              ),
                                              child: Center(child: Icon(IcoFontIcons.minus,color: Colors.white,size: size.convertLongestSide(context, 20),)),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.symmetric(horizontal: 20),
                                            alignment: Alignment.center,
                                            height: size.convertLongestSide(context, 33),
                                            child: RichText(
                                              text: TextSpan(
                                                  text: "${item[index]["count"]}",
                                                  style: styles.PoppinsSemiBold(
                                                      fontSize: size.convertLongestSide(context, 18)
                                                  )
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: (){
                                              setState(() {
                                                item[index]["count"]++;
                                              });
                                            },
                                            child: Container(
                                              width: size.convertLongestSide(context, 33),
                                              height: size.convertLongestSide(context, 33),
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(5),
                                                  color: appColor
                                              ),
                                              child: Center(child: Icon(IcoFontIcons.plus,color: Colors.white,size: size.convertLongestSide(context, 20),)),
                                            ),
                                          ),
                                        ],),
                                    ),
                                  ],
                                ),),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index){
                      return SizedBox(height: size.convertLongestSide(context, 17),);
                    },
                    itemCount: item.length,
                  ),
                ],
              ),
            ),
            backButton(
              shadowColor: Colors.black.withOpacity(0.1),
              istitle: true,
              title: "Tú pedido",
              backTap: (){
                Navigator.pop(context);
              },

            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: size.convertLongestSide(context, 20)),
              alignment: Alignment.bottomCenter,
              height: MediaQuery.of(context).size.height,
              child: filledButton(
                onTap: (){
                  Navigator.push(context, PageTransition(child: deliveryAddress(),
                  type: PageTransitionType.leftToRight));
                },
                leftWidget: Container(
                  padding: EdgeInsets.only(left: size.convertWidth(context, 30)),
                  child: Text("continuar",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize:  size.convertLongestSide(context, 17) ,
                      fontFamily:  "Poppins-Medium" ,
                    ),
                  ),
                ),
                rigtWidget: Container(
                  padding: EdgeInsets.only(right: size.convertWidth(context, 30)),
                  child: Expanded(
                    child: Text("\$200.00",
                      maxLines: 1,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize:  size.convertLongestSide(context, 17),
                        fontFamily:  "Poppins-Medium" ,
                      ),
                    ),
                  ),
                ),
              ),
            ),

          ],
        ),

    );
  }
}
