import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shop_maquetacion/UIDesign/card.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomBottomAppBar.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomDrawer.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
class product extends StatefulWidget {
  @override
  _productState createState() => _productState();
}

class _productState extends State<product> {
  openDrawer() {
    return Drawer(
      child: CustomDrawer(),
    );
  }
  int count = 2;
  bool like = true;
  GlobalKey<ScaffoldState> scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldkey,
      drawer: openDrawer(),
      body: body(),
      bottomNavigationBar: CustomBottomBar(
        select: 6,
        scaffoldkey: scaffoldkey,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked, //specify the location of the FAB
      floatingActionButton: Container(
        child: GestureDetector(
          onTap: () {
            print("floatingActionButton");
          },
          child: Container(
            padding: EdgeInsets.all(10),
            height: size.convertLongestSide(context, 80),
            width: size.convertLongestSide(context, 80),
            child: FloatingActionButton(
              backgroundColor: buttonColor,
              elevation: 25,

              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                          text: "${count}",
                          style: styles.PoppinsSemiBold(
                              fontSize: size.convertLongestSide(context, 28),
                            color: Colors.white
                          )
                      ),
                    )

                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
  body(){
    return Container(
      child:  Stack(
          children: <Widget>[

             SingleChildScrollView(
               child: Stack(
                 children: <Widget>[
                   Image.asset("assets/icons/product.png",),
                   Column(
                     crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: size.convertLongestSide(context, 280),),
                        Center(
                          child: Container(
                            margin: EdgeInsets.only(top: size.convertLongestSide(context, 6),
                            bottom: size.convertLongestSide(context, 6)),
                            width: size.convertWidth(context, 364),
                            height: size.convertLongestSide(context, 288),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black.withOpacity(0.05),
                                  blurRadius: 5,
                                  spreadRadius: 2
                                )
                              ]
                            ),
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: size.convertLongestSide(context, 24),),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 18)),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                    Container(
                                      child: RichText(
                                        text: TextSpan(
                                          text: "Plátano Tabasco",
                                          style: styles.PoppinsSemiBold(
                                            fontSize: size.convertLongestSide(context, 20)
                                          )
                                        ),
                                      ),
                                    ),
                                      Container(
                                        child: RichText(
                                          text: TextSpan(
                                              text: "\$100",
                                              style: styles.PoppinsSemiBold(
                                                  fontSize: size.convertLongestSide(context, 20)
                                              )
                                          ),
                                        ),
                                      )
                                  ],),
                                ),

                                SizedBox(height: size.convertLongestSide(context, 33),),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 80)),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      GestureDetector(
                                        onTap: (){
                                          if(count>1){
                                            setState(() {
                                              count--;
                                            });
                                          }
                                        },
                                        child: Container(
                                          width: size.convertLongestSide(context, 49),
                                          height: size.convertLongestSide(context, 49),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            color: appColor
                                          ),
                                          child: Center(child: Icon(IcoFontIcons.minus,color: Colors.white,)),
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.center,
                                        height: size.convertLongestSide(context, 49),
                                        child: RichText(
                                          text: TextSpan(
                                              text: "${count}",
                                              style: styles.PoppinsSemiBold(
                                                  fontSize: size.convertLongestSide(context, 28)
                                              )
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: (){
                                          setState(() {
                                            count++;
                                          });
                                        },
                                        child: Container(
                                          width: size.convertLongestSide(context, 49),
                                          height: size.convertLongestSide(context, 49),
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(10),
                                              color: appColor
                                          ),
                                          child: Center(child: Icon(IcoFontIcons.plus,color: Colors.white,)),
                                        ),
                                      ),
                                    ],),
                                ),
                                SizedBox(height: size.convertLongestSide(context, 33),),
                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 12)),
                                  width: size.convertWidth(context, 364),
                                  child: Wrap(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.symmetric(horizontal: 15,vertical: 2),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(100),
                                          border: Border.all(
                                            width: 1, color: buttonColor,
                                          )
                                        ),
                                        child: RichText(
                                          text: TextSpan(
                                              text: "100 gms",
                                              style: styles.PoppinsSemiBold(
                                                  fontSize: size.convertLongestSide(context, 12)
                                              )
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: size.convertLongestSide(context, 9),),
                                      Container(
                                        padding: EdgeInsets.symmetric(horizontal: 15,vertical: 2),
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(100),
                                            border: Border.all(
                                              width: 1, color: buttonColor,
                                            )
                                        ),
                                        child: RichText(
                                          text: TextSpan(
                                              text: "100 gms",
                                              style: styles.PoppinsSemiBold(
                                                  fontSize: size.convertLongestSide(context, 12)
                                              )
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(height: size.convertLongestSide(context, 33),),

                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 12)),
                                  width: size.convertWidth(context, 364),
                                  child:  RichText(
                                    maxLines: 2,
                                    text: TextSpan(
                                        text: "Plátano tabasco, 100 calidad nacional de exportación fresco de temporada",
                                        style: styles.PoppinsSemiBold(
                                            fontSize: size.convertLongestSide(context, 14)
                                        )
                                    ),
                                  ),
                                ),

                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: size.convertLongestSide(context, 33),),
                        filledButton(
                          onTap: (){
                            Navigator.push(context, PageTransition(child: card(),
                              type: PageTransitionType.leftToRight
                            ));
                          },
                          leftWidget: Container(
                            padding: EdgeInsets.only(left: size.convertWidth(context, 30)),
                            child: Text("Agregar al carrito",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize:  size.convertLongestSide(context, 17) ,
                                fontFamily:  "Poppins-Medium" ,
                              ),
                            ),
                          ),
                          rigtWidget: Container(
                            padding: EdgeInsets.only(right: size.convertWidth(context, 30)),
                            child: Expanded(
                              child: Text("\$500.00",
                                maxLines: 1,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize:  size.convertLongestSide(context, 17),
                                  fontFamily:  "Poppins-Medium" ,
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: size.convertLongestSide(context, 35),),
                      ],
                    ),
                 ],
               ),
             ),
            backButton(
              shadowColor: Colors.black.withOpacity(0.1),
              isTrallingIcon: true,
              backTap: (){
                Navigator.pop(context);
              },
              likeTap: (){
                setState(() {
                  like = !like;
                });
              },
              trallingIcon: !like ? Image.asset("assets/icons/heart.png",color: Colors.grey,) : Image.asset("assets/icons/heart.png",color: Colors.red,),
            ),


          ],
        ),

    );
  }
}
