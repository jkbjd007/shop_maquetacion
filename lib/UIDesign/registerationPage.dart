import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shop_maquetacion/UIDesign/forgotPassword.dart';
import 'package:shop_maquetacion/UIDesign/homePage.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomTextField.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/repeatedWigets/radioButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
class registerationPage extends StatefulWidget {
  @override
  _registerationPageState createState() => _registerationPageState();
}

class _registerationPageState extends State<registerationPage> {
  bool hidePass = true;


  @override
  Widget build(BuildContext context) {
    return Scaffold(body:  body(),);
  }
  body(){
    return Container(
      child: Stack(
        children: <Widget>[
          Container(child:SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: size.convertLongestSide(context, 120),),
                Center(child: CustomTextField(
                  hints: "Nombre",
                  iconWidget: Image.asset("assets/icons/user.png"),
                )),

                SizedBox(height: size.convertLongestSide(context, 20),),
                Center(child: CustomTextField(
                  hints: "Apellido",
                  iconWidget: Image.asset("assets/icons/user.png"),
                )),

                SizedBox(height: size.convertLongestSide(context, 20),),
                Center(child: CustomTextField(
                  hints: "Teléfono",
                  iconWidget: Image.asset("assets/icons/tel.png"),
                )),

                SizedBox(height: size.convertLongestSide(context, 20),),
                Center(child: CustomTextField(
                  hints: "Email",
                  iconWidget: Image.asset("assets/icons/email.png"),
                )),

                SizedBox(height: size.convertLongestSide(context, 20),),
                Center(child: GestureDetector(
                  onTap: (){
                    setState(() {
                      hidePass = !hidePass;
                    });
                  },
                  child: CustomTextField(
                    obscureText: hidePass,
                    hints: "Contraseña",
                    iconWidget: Image.asset("assets/icons/lock.png"),
                    trailingIcon: hidePass ? Icon(IcoFontIcons.eyeBlocked,size: size.convertLongestSide(context, 35),) : Icon(IcoFontIcons.eye,size: size.convertLongestSide(context, 35),) ,

                  ),
                )),

                SizedBox(height: size.convertLongestSide(context, 20),),
                Center(child: filledButton(
                  onTap: (){
                    setState(() {
                      //selectCityPage = true;
                    });
                  },
                  txt: "Registrarse",
                )),

                SizedBox(height: size.convertLongestSide(context, 25),),
                Center(child: Container(
                  width: size.convertWidth(context, 232),
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        text: "  Al registrarte estás aceptando nuestros términos y condiciones ",
                        style: styles.PoppinsMedium(
                            color: BlackFont,
                          fontSize: size.convertLongestSide(context, 11)
                        )
                    ),
                  ),
                )),



                SizedBox(height: size.convertLongestSide(context, 10),),
                Center(child: Image.asset("assets/icons/fruits.png")),

                SizedBox(height: size.convertLongestSide(context, 10),),

              ],
            ),
          ),
          ),
          backButton(
            title: "Crea tu cuenta",
            istitle: true,
            backTap: (){
              Navigator.pop(context);
            },
          )
        ],
      ),
    );
  }

}
