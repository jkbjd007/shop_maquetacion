import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shop_maquetacion/UIDesign/card.dart';
import 'package:shop_maquetacion/UIDesign/followPage.dart';
import 'package:shop_maquetacion/UIDesign/forgotPassword.dart';
import 'package:shop_maquetacion/UIDesign/homePage.dart';
import 'package:shop_maquetacion/UIDesign/product.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomBottomAppBar.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomDrawer.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomTextField.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/repeatedWigets/radioButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/sizeConfig.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class thankYouPage extends StatefulWidget {
  @override
  _thankYouPageState createState() => _thankYouPageState();
}

class _thankYouPageState extends State<thankYouPage> {

  openDrawer() {
    return Drawer(
      child: CustomDrawer(),
    );
  }
  GlobalKey<ScaffoldState> scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldkey,
      drawer: openDrawer(),
      body:  body(),
      bottomNavigationBar: CustomBottomBar(
        select: 10,
        scaffoldkey: scaffoldkey,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked, //specify the location of the FAB
      floatingActionButton: Container(
        child: GestureDetector(
          onTap: () {
            print("floatingActionButton");
            Navigator.push(context, PageTransition(child: card(),type: PageTransitionType.leftToRight));
          },
          child: Container(
            padding: EdgeInsets.all(10),
            height: size.convertLongestSide(context, 80),
            width: size.convertLongestSide(context, 80),
            child: FloatingActionButton(
              backgroundColor: buttonColor,
              elevation: 25,
              //onPressed: _onFloatingPress,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset("assets/icons/cardCare.png")

                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }





  body() {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: size.convertLongestSide(context, 50),),
           Container(
             margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 61)),
               child: Image.asset("assets/icons/colorLogo.png")),
            Center(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                Image.asset("assets/icons/backGround.png"),
                Image.asset("assets/icons/thankMan.png"),
              ],),
            ),
            SizedBox(height: size.convertLongestSide(context, 10),),
            Center(
              child: Container(child: RichText(
                text: TextSpan(
                  text: "¡gracias",
                  style: styles.PoppinsMedium(
                    fontSize: size.convertLongestSide(context, 25),
                    color: Color(0xff05122c),
                  )
                ),
              ),),
            ),
            Center(
              child: Container(child: RichText(
                text: TextSpan(
                    text: "por tu compra!",
                    style: styles.PoppinsBlack(
                      fontSize: size.convertLongestSide(context, 25),
                      color: Color(0xff05122c),
                    )
                ),
              ),),
            ),
            SizedBox(height: size.convertLongestSide(context, 10),),
            Center(
              child: filledButton(
                txt: "Sigue tu órden",
                onTap: (){
                  Navigator.push(context, PageTransition(
                    child: followPage(),
                    type: PageTransitionType.leftToRight
                  ));
                },
              ),
            ),
            SizedBox(height: size.convertLongestSide(context, 30),),
          ],
        ),
      ),
    );
  }


}
