import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shop_maquetacion/UIDesign/card.dart';
import 'package:shop_maquetacion/UIDesign/forgotPassword.dart';
import 'package:shop_maquetacion/UIDesign/homePage.dart';
import 'package:shop_maquetacion/UIDesign/product.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomBottomAppBar.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomDrawer.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomTextField.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/repeatedWigets/radioButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/sizeConfig.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class homePage extends StatefulWidget {
  @override
  _homePageState createState() => _homePageState();
}

class _homePageState extends State<homePage> {
  PageController _controller = PageController(
    initialPage: 0,
  );

  List items = [
    {
      "icon":"assets/icons/Frutas.png",
          "name":"Frutas"
    },

    {
      "icon":"assets/icons/Verduras.png",
      "name":"Verduras"
    },

    {
      "icon":"assets/icons/Carnes.png",
      "name":"Carnes"
    },

    {
      "icon":"assets/icons/Frutas1.png",
      "name":"Frutas"
    },

    {
      "icon":"assets/icons/Frutas.png",
      "name":"Frutas"
    },

    {
      "icon":"assets/icons/Verduras.png",
      "name":"Verduras"
    },

    {
      "icon":"assets/icons/Carnes.png",
      "name":"Carnes"
    },

    {
      "icon":"assets/icons/Frutas1.png",
      "name":"Frutas"
    },


  ];
  List sliderImage = [
    "assets/icons/slider1.jpg",
    "assets/icons/slider1.jpg",
    "assets/icons/slider1.jpg",
  ];
  List fruid = [
    {
      "icon":"assets/icons/Platano.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Platano"
    },
    {
      "icon":"assets/icons/Arandano.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Arandano"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Manzana.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Manzana"
    }
  ];
  List fruid1 = [
    {
      "icon":"assets/icons/Platano.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Platano"
    },
    {
      "icon":"assets/icons/Arandano.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Arandano"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Manzana.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Manzana"
    },
    {
      "icon":"assets/icons/Platano.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Platano"
    },
    {
      "icon":"assets/icons/Arandano.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Arandano"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Durazno.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Durazno"
    },
    {
      "icon":"assets/icons/Manzana.png",
      "price":"\$ 100",
      "quantity":"kg",
      "name":"Manzana"
    }
  ];
  bool detailBody = false;
  GlobalKey<ScaffoldState> scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldkey,
      drawer: openDrawer(),
      body: detailBody? itemDetailBody() : body(),
        bottomNavigationBar: CustomBottomBar(
          select: 2,
          scaffoldkey: scaffoldkey,
        ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked, //specify the location of the FAB
      floatingActionButton: Container(
        child: GestureDetector(
          onTap: () {
            print("floatingActionButton");
            Navigator.push(context, PageTransition(child: card(),type: PageTransitionType.leftToRight));
          },
          child: Container(
            padding: EdgeInsets.all(10),
            height: size.convertLongestSide(context, 80),
            width: size.convertLongestSide(context, 80),
            child: FloatingActionButton(
              backgroundColor: buttonColor,
              elevation: 25,
              //onPressed: _onFloatingPress,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset("assets/icons/cardCare.png")

                  ],
                ),
              ),
            ),
          ),
        ),
      ),

    );
  }
  openDrawer() {
    return Drawer(
      child: CustomDrawer(),
    );
  }

  topPortion() {
    return Container(
      margin: EdgeInsets.only(
          left: size.convertWidth(context, 33),
          right: size.convertWidth(context, 11)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Image.asset("assets/icons/colorLogo.png"),
          SizedBox(
            width: size.convertWidth(context, 30),
          ),
          Expanded(
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Image.asset("assets/icons/profile.png"),
                              SizedBox(
                                width: size.convertWidth(context, 7),
                              ),
                              RichText(
                                text: TextSpan(
                                    text: "Rolfo Romero",
                                    style: styles.PoppinsRegular(
                                        fontSize: size.convertLongestSide(
                                            context, 12))),
                              )
                            ],
                          ),
                        ),
                        Container(
                          child: Row(
                            children: <Widget>[
                              Image.asset("assets/icons/cerrarSesion.png"),
                              SizedBox(
                                width: size.convertWidth(context, 7),
                              ),
                              Image.asset("assets/icons/wattsapp.png"),
                              SizedBox(
                                width: size.convertWidth(context, 7),
                              ),
                              Image.asset("assets/icons/facebook.png"),
                              SizedBox(
                                width: size.convertWidth(context, 7),
                              ),
                              Image.asset("assets/icons/instagram.png"),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: size.convertLongestSide(context, 20),
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        child: RichText(
                          text: TextSpan(
                              text: "Entregar en:",
                              style: styles.PoppinsRegular(
                                  fontSize:
                                      size.convertLongestSide(context, 12))),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: RichText(
                          text: TextSpan(
                              text: "Agrarismo 63 col Escandón",
                              style: styles.PoppinsRegular(
                                  fontSize:
                                      size.convertLongestSide(context, 12))),
                        ),
                      ),
                      Container(
                        width: size.convertLongestSide(context, 20),
                        height: size.convertLongestSide(context, 20),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: buttonColor),
                        child: Center(
                          child: Image.asset("assets/icons/mark.png"),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: size.convertLongestSide(context, 10),
                  ),
                  Container(
                      child: CustomTextField(
                    height1: size.convertLongestSide(context, 40),
                    iconWidget: Image.asset("assets/icons/IconSearch.png"),
                    hints: "buscar productos",
                    backGroundColor: Color(0xffd4d4e0).withOpacity(0.4),
                    //hintsColor: BlackFont,
                  ))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  istPortion() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: size.convertWidth(context, 20)),
            child: Row(
              children: <Widget>[
                Image.asset("assets/icons/fair.png"),
                SizedBox(
                  width: size.convertHeight(context, 12),
                ),
                RichText(
                  text: TextSpan(
                      text: "Promociones",
                      style: styles.PoppinsSemiBold(
                          fontSize: size.convertLongestSide(context, 15))),
                )
              ],
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              height: size.convertLongestSide(context, 180),
              margin:
              EdgeInsets.only(top: size.convertLongestSide(context, 12)),
              child: Stack(
                children: <Widget>[
                  PageView.builder(
                      controller: _controller,
                      itemCount: sliderImage.length,
                      allowImplicitScrolling: true,
                      itemBuilder: (context, index) {
                        return Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: size.convertWidth(context, 20)),
                            child: Image.asset(sliderImage[index]));
                      }),
                  Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: size.convertWidth(context, 5)),
                    width: MediaQuery.of(context).size.width,
                    height: size.convertLongestSide(context, 180),
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            _controller.previousPage(
                                duration: Duration(seconds: 1),
                                curve: Curves.linear);
                          },
                          child: Container(
                            width: size.convertLongestSide(context, 40),
                            height: size.convertLongestSide(context, 40),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(40),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    //color: Colors.green,
                                    color: Colors.grey.withOpacity(0.3),
//                                      blurRadius: 10.0,
//                                      spreadRadius: 7,
                                    offset: Offset(-2.0, 2.0),
                                  ),
                                ]),
                            child: Center(
                              child: Icon(
                                Icons.arrow_back_ios,
                                color: IconsColor,
                                size: size.convertLongestSide(context, 30),
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            _controller.nextPage(
                                duration: Duration(seconds: 1),
                                curve: Curves.linear);
                          },
                          child: Container(
                            width: size.convertLongestSide(context, 40),
                            height: size.convertLongestSide(context, 40),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(40),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    //color: Colors.green,
                                    color: Colors.grey.withOpacity(0.3),
//                                      blurRadius: 10.0,
//                                      spreadRadius: 7,
                                    offset: Offset(2.0, -1.0),
                                  ),
                                ]),
                            child: Center(
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: IconsColor,
                                size: size.convertLongestSide(context, 30),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
//          Container(child: Stack(
//            children: <Widget>[
//
//            ],
//          ),)
        ],
      ),
    );
  }

  secondPortion() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: size.convertWidth(context, 20)),
            child: Row(
              children: <Widget>[
                Image.asset("assets/icons/cesta.png"),
                SizedBox(
                  width: size.convertHeight(context, 12),
                ),
                RichText(
                  text: TextSpan(
                      text: "Productos",
                      style: styles.PoppinsSemiBold(
                          fontSize: size.convertLongestSide(context, 15))),
                )
              ],
            ),
          ),
          SizedBox(height: size.convertLongestSide(context, 15),),
          Container(
            margin: MediaQuery.of(context).orientation == Orientation.landscape ? EdgeInsets.symmetric(horizontal: size.convertWidth(context, 0)) : EdgeInsets.symmetric(horizontal: size.convertWidth(context, 14)),
            child: StaggeredGridView.countBuilder(
              padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
              shrinkWrap: true,
              physics: ScrollPhysics(),
              crossAxisCount: 4,
              itemCount: items.length,
              itemBuilder: (BuildContext context, int index) =>
               GestureDetector(
                 onTap: (){
                   if(index == 0 ||index == 3 || index == 4 ||index == 7)
                   setState(() {
                     detailBody = true;
                   });
                 },
                 child: Container(
                    child:  Column(
                     children: <Widget>[
                       Image.asset(items[index]["icon"]),
                       Container(
                         padding: EdgeInsets.symmetric(horizontal: 8),
                         child: RichText(
                           maxLines: 2,
                           textAlign: TextAlign.center,
                           text: TextSpan(
                             text: items[index]["name"],
                             style: styles.PoppinsBlack(
                               fontSize: size.convertLongestSide(context, 9)
                             )
                           ),
                         ),
                       )
                     ],
                    )
              ),
               ),
              staggeredTileBuilder: (int index) =>
              new StaggeredTile.fit(1),
              mainAxisSpacing: 15,
             crossAxisSpacing: 15,
            ),
          )
        ],
      ),
    );
  }

  thirdPortion() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: size.convertWidth(context, 20)),
            child: Row(
              children: <Widget>[
                Image.asset("assets/icons/mejorVendido.png"),
                SizedBox(
                  width: size.convertHeight(context, 12),
                ),
                RichText(
                  text: TextSpan(
                      text: "Lo mejor vendido esta semana",
                      style: styles.PoppinsSemiBold(
                          fontSize: size.convertLongestSide(context, 15))),
                )
              ],
            ),
          ),
          SizedBox(height: size.convertLongestSide(context, 15),),

          Container(
            height: size.convertLongestSide(context, 135),
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 14)),

            //color: Colors.lightBlueAccent,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    separatorBuilder: (context,int index){
                      return SizedBox(width: size.convertWidth(context, 12),);
                    },
                    padding: EdgeInsets.symmetric(vertical: 0.0),
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemCount: fruid.length,
                    itemBuilder: (BuildContext context, int index) =>
                        GestureDetector(
                          onTap: (){

                          },
                          child: Container(
                            width: size.convertLongestSide(context, 100),
                            margin: EdgeInsets.symmetric(vertical: 4,horizontal: 0),
                            padding: EdgeInsets.only(left: 8,right: 8,top: 8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                              boxShadow: [ //background color of box
                                BoxShadow(
                                    color: Colors.grey.withOpacity(0.09),
                                    spreadRadius: 2,
                                    blurRadius: 5,
                                    offset: Offset(-1,-1),

                                )
                              ]
                            ),
                              child:  Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Image.asset(fruid[index]["icon"]),
                                   SizedBox(height: size.convertLongestSide(context, 10),),
                                   Container(
                                     padding: EdgeInsets.symmetric(horizontal: 4),
                                     child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(

                                            child: RichText(
                                              maxLines: 1,
                                              textAlign: TextAlign.center,
                                              text: TextSpan(
                                                  text: fruid[index]["price"],
                                                  style: styles.PoppinsSemiBold(
                                                      fontSize: size.convertLongestSide(context, 8)
                                                  )
                                              ),
                                            ),
                                          ),
                                          Container(

                                            child: RichText(
                                              maxLines: 1,
                                              textAlign: TextAlign.center,
                                              text: TextSpan(
                                                  text: fruid[index]["quantity"],
                                                  style: styles.PoppinsSemiBold(
                                                      fontSize: size.convertLongestSide(context, 8)
                                                  )
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                   ),
                                  Container(
                                    padding: EdgeInsets.symmetric(horizontal: 4),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: RichText(
                                              maxLines: 1,
                                              textAlign: TextAlign.center,
                                              text: TextSpan(
                                                  text: fruid[index]["name"],
                                                  style: styles.PoppinsBlack(
                                                      fontSize: size.convertLongestSide(context, 9)
                                                  )
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              )
                          ),
                        ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  body() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: size.convertLongestSide(context, 40),
            ),
            topPortion(),
            SizedBox(
              height: size.convertLongestSide(context, 20),
            ),
            istPortion(),
            SizedBox(
              height: size.convertLongestSide(context, 20),
            ),
            secondPortion(),
            SizedBox(
              height: size.convertLongestSide(context, 5),///becouse 15 secondPortion
            ),
            thirdPortion(),
            SizedBox(
              height: size.convertLongestSide(context, 20),
            ),
          ],
        ),
      ),
    );
  }
  
  itemDetailBody(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convertLongestSide(context, 25),),
          Center(child: Image.asset("assets/icons/detailLogo.png")),
            SizedBox(height: size.convertLongestSide(context, 25),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 23)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                Container(child: Row(children: <Widget>[
                  Container(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset("assets/icons/fruidIcon.png"),
                      ),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: appColor
                  ),),
                  SizedBox(width: size.convertLongestSide(context, 3),),
                  Container(
                    //color: Colors.indigoAccent,
                    width: size.convertWidth(context, 70),
                    child: RichText(
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text: "Frutas",
                          style: styles.PoppinsSemiBold(
                              fontSize: size.convertLongestSide(context, 16)
                          )
                      ),
                    ),
                  ),
                ],),),

                Container(child: Row(children: <Widget>[
                  Container(
                    child: CustomTextField(
                      height1: size.convertLongestSide(context, 40),
                      width1: size.convertWidth(context, 204),
                      iconWidget: Image.asset("assets/icons/IconSearch.png"),
                      hints: "buscar productos",
                      backGroundColor: Color(0xffd4d4e0).withOpacity(0.4),
                      //hintsColor: BlackFont,
                    ),
                    decoration: BoxDecoration(

                    ),),
                  SizedBox(width: size.convertLongestSide(context, 8),),
                  Container(
                    child: Image.asset("assets/icons/lista.png"),
                  ),
                ],),)


              ],),
            ),
            SizedBox(height: size.convertLongestSide(context, 25),),
            Container(
              margin: MediaQuery.of(context).orientation == Orientation.landscape ? EdgeInsets.symmetric(horizontal: size.convertWidth(context, 0)) : EdgeInsets.symmetric(horizontal: size.convertWidth(context, 14)),
              child: StaggeredGridView.countBuilder(
                padding: EdgeInsets.symmetric(vertical: 0.0,horizontal: 0.0),
                shrinkWrap: true,
                physics: ScrollPhysics(),
                crossAxisCount: 3,
                itemCount: fruid1.length,
                itemBuilder: (BuildContext context, int index) =>
                    Container(
                        //width: size.convertLongestSide(context, 80),
                        margin: EdgeInsets.symmetric(vertical: 4,horizontal: 0),
                        padding: EdgeInsets.only(left: 8,right: 8,top: 8),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                            boxShadow: [ //background color of box
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.09),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset: Offset(-1,-1),

                              )
                            ]
                        ),
                        child:  Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(fruid1[index]["icon"]),
                            SizedBox(height: size.convertLongestSide(context, 10),),
                            Container(
                              //color: Colors.green,
                              width: size.convertLongestSide(context, 80),
                              child: Row(children: <Widget>[
                              Expanded(
                                child: Container(child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.symmetric(horizontal: 4),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Expanded(
                                            child: Container(
                                              alignment: Alignment.centerLeft,
                                              //color:Colors.red,
                                              child: RichText(
                                                maxLines: 1,
                                                //textAlign: TextAlign.center,
                                                text: TextSpan(
                                                    text: fruid1[index]["price"],
                                                    style: styles.PoppinsSemiBold(
                                                        fontSize: size.convertLongestSide(context, 8)
                                                    )
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            child: RichText(
                                              maxLines: 1,
                                              textAlign: TextAlign.center,
                                              text: TextSpan(
                                                  text: fruid1[index]["quantity"],
                                                  style: styles.PoppinsSemiBold(
                                                      fontSize: size.convertLongestSide(context, 8)
                                                  )
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.symmetric(horizontal: 4),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Expanded(
                                            child: Container(
                                              alignment: Alignment.centerLeft,
                                              child: RichText(
                                                maxLines: 2,
                                                //textAlign: TextAlign.center,
                                                text: TextSpan(
                                                    text: fruid1[index]["name"],
                                                    style: styles.PoppinsBlack(
                                                        fontSize: size.convertLongestSide(context, 9)
                                                    )
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),),
                              ),
                                Container(
                                  padding: EdgeInsets.all(4),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: buttonColor
                                  ),
                                  child: Image.asset("assets/icons/cardCareSmall.png"),
                                )
                              ],
                              ),
                            )
                          ],
                        )
                    ),
                staggeredTileBuilder: (int index) =>
                new StaggeredTile.fit(1),
                mainAxisSpacing: 15,
                crossAxisSpacing: 15,
              ),
            ),
            SizedBox(height: size.convertLongestSide(context, 25),),
        ],),
      ),
    );
  }
}
