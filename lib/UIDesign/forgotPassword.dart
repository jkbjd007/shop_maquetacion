import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shop_maquetacion/UIDesign/forgotPassword.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomTextField.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
class forgotPassword extends StatefulWidget {
  @override
  _forgotPasswordState createState() => _forgotPasswordState();
}

class _forgotPasswordState extends State<forgotPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: body(),);
  }
  body(){
    return Container(
      child: Stack(
        children: <Widget>[
          Container(child:SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: size.convertLongestSide(context, 125),),
                Center(child: Image.asset("assets/icons/forgotPasswordLogo.png")),
                SizedBox(height: size.convertLongestSide(context, 40),),
                Center(child: RichText(
                  text: TextSpan(
                      text: "Ingresa tu correo electrónico",
                      style: styles.PoppinsMedium(
                          color: BlackFont
                      )
                  ),
                )),

                SizedBox(height: size.convertLongestSide(context, 20),),
                Center(child: CustomTextField(
                  hints: "Email",
                  iconWidget: Image.asset("assets/icons/email.png"),
                )),



                SizedBox(height: size.convertLongestSide(context, 40),),
                Center(child: filledButton(
                  onTap: (){},
                  txt: "Enviar",
                )),

                SizedBox(height: size.convertLongestSide(context, 10),),

              ],
            ),
          ),
          ),

          backButton(
            title: "Restablece túcontraseña",
            istitle: true,
            backTap: (){Navigator.pop(context);},
          ),
        ],
      ),
    );
  }
}
