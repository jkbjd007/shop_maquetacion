import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shop_maquetacion/UIDesign/card.dart';
import 'package:shop_maquetacion/UIDesign/forgotPassword.dart';
import 'package:shop_maquetacion/UIDesign/homePage.dart';
import 'package:shop_maquetacion/UIDesign/product.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomBottomAppBar.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomDrawer.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomTextField.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/repeatedWigets/radioButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/sizeConfig.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class followPage extends StatefulWidget {
  @override
  _followPageState createState() => _followPageState();
}

class _followPageState extends State<followPage> {
  openDrawer() {
    return Drawer(
      child: CustomDrawer(),
    );
  }

  GlobalKey<ScaffoldState> scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldkey,
      drawer: openDrawer(),
      body:  body(),
      bottomNavigationBar: CustomBottomBar(
        select: 10,
        scaffoldkey: scaffoldkey,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked, //specify the location of the FAB
      floatingActionButton: Container(
        child: GestureDetector(
          onTap: () {
            print("floatingActionButton");
            Navigator.push(context, PageTransition(child: card(),type: PageTransitionType.leftToRight));
          },
          child: Container(
            padding: EdgeInsets.all(10),
            height: size.convertLongestSide(context, 80),
            width: size.convertLongestSide(context, 80),
            child: FloatingActionButton(
              backgroundColor: buttonColor,
              elevation: 25,
              //onPressed: _onFloatingPress,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset("assets/icons/cardCare.png")

                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }





  body() {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(child:SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: size.convertLongestSide(context, 90),),
              Container(
                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 50)),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            RichText(
                              text: TextSpan(
                                text: "¿Tienes alguna idicación?",
                                style: styles.PoppinsSemiBold(
                                  fontSize: size.convertLongestSide(context, 19)
                                )
                              ),
                            ),
                            RichText(
                              text: TextSpan(
                                  text: "mandanos un mensaje",
                                  style: styles.PoppinsMedium(
                                      fontSize: size.convertLongestSide(context, 15),
                                    color: Color(0xff05122c)
                                  )
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Image.asset("assets/icons/whatsapp.png")
                  ],
                ),
              ),

                SizedBox(height: size.convertLongestSide(context, 20),),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 50)),
                  child: Divider(
                    color: appColor,
                    height: 2,
                  ),
                ),

                SizedBox(height: size.convertLongestSide(context, 15),),

                Container(
                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 50)),
                  child: Row(
                    children: <Widget>[
                      radioButton(
                        selected: true,
                      ),
                      SizedBox(width: size.convertWidth(context, 23),),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            RichText(
                              text: TextSpan(
                                  text: "Orden Recibida",
                                  style: styles.PoppinsMedium(
                                      fontSize: size.convertLongestSide(context, 17),
                                      color: Color(0xff05122c)
                                  )
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[

                                RichText(
                                  text: TextSpan(
                                      text: "12/08/2020",
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(context, 14),
                                          color: Color(0xff3a3d4e)
                                      )
                                  ),
                                ),
                                SizedBox(width: size.convertWidth(context, 23),),
                                RichText(
                                  text: TextSpan(
                                      text: "21:00 hrs",
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(context, 14),
                                          color: Color(0xff3a3d4e)
                                      )
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),),
                Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 61)),
                      child: Image.asset("assets/icons/Line54.png"),
                    ),
                  ],
                ),

                Container(
                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 50)),
                  child: Row(
                    children: <Widget>[
                      radioButton(
                        selected: false,
                      ),
                      SizedBox(width: size.convertWidth(context, 23),),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            RichText(
                              text: TextSpan(
                                  text: "Preparando",
                                  style: styles.PoppinsMedium(
                                      fontSize: size.convertLongestSide(context, 17),
                                      color: Color(0xff05122c)
                                  )
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[

                                RichText(
                                  text: TextSpan(
                                      text: "12/08/2020",
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(context, 14),
                                          color: Color(0xff3a3d4e)
                                      )
                                  ),
                                ),
                                SizedBox(width: size.convertWidth(context, 23),),
                                RichText(
                                  text: TextSpan(
                                      text: "21:00 hrs",
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(context, 14),
                                          color: Color(0xff3a3d4e)
                                      )
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),),
                Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 61)),
                      child: Image.asset("assets/icons/Line54.png"),
                    ),
                  ],
                ),

                Container(
                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 50)),
                  child: Row(
                    children: <Widget>[
                      radioButton(
                        selected: false,
                      ),
                      SizedBox(width: size.convertWidth(context, 23),),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            RichText(
                              text: TextSpan(
                                  text: "En camino",
                                  style: styles.PoppinsMedium(
                                      fontSize: size.convertLongestSide(context, 17),
                                      color: Color(0xff05122c)
                                  )
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[

                                RichText(
                                  text: TextSpan(
                                      text: "12/08/2020",
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(context, 14),
                                          color: Color(0xff3a3d4e)
                                      )
                                  ),
                                ),
                                SizedBox(width: size.convertWidth(context, 23),),
                                RichText(
                                  text: TextSpan(
                                      text: "21:00 hrs",
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(context, 14),
                                          color: Color(0xff3a3d4e)
                                      )
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),),
                Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 61)),
                      child: Image.asset("assets/icons/Line54.png"),
                    ),
                  ],
                ),

                Container(
                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 50)),
                  child: Row(
                    children: <Widget>[
                      radioButton(
                        selected: false,
                      ),
                      SizedBox(width: size.convertWidth(context, 23),),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            RichText(
                              text: TextSpan(
                                  text: "Entregado",
                                  style: styles.PoppinsMedium(
                                      fontSize: size.convertLongestSide(context, 17),
                                      color: Color(0xff05122c)
                                  )
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[

                                RichText(
                                  text: TextSpan(
                                      text: "12/08/2020",
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(context, 14),
                                          color: Color(0xff3a3d4e)
                                      )
                                  ),
                                ),
                                SizedBox(width: size.convertWidth(context, 23),),
                                RichText(
                                  text: TextSpan(
                                      text: "21:00 hrs",
                                      style: styles.PoppinsMedium(
                                          fontSize: size.convertLongestSide(context, 14),
                                          color: Color(0xff3a3d4e)
                                      )
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),),
                SizedBox(height: size.convertWidth(context, 20),),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 50)),
                  child: Row(
                    children: <Widget>[
                      Image.asset("assets/icons/mark1.png"),
                      SizedBox(width: size.convertWidth(context, 23),),
                      Container(
                        child: RichText(
                          text: TextSpan(
                              text: "Agrarismo #63 col Escandón",
                              style: styles.PoppinsMedium(
                                  fontSize: size.convertLongestSide(context, 17),
                                  color: Color(0xff05122c)
                              )
                          ),
                        ),
                      )
                    ],
                  ),),

                SizedBox(height: size.convertWidth(context, 15),),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 15),vertical: size.convertLongestSide(context, 10)),
                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 25)),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.05),
                            blurRadius: 2,
                            spreadRadius: 2,
                            offset: Offset(3,3)
                        )
                      ]
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(child: Image.asset("assets/icons/Durazno.png"),),
                      SizedBox(width: size.convertWidth(context, 12),),
                      Expanded(
                        child: Container(child: Column(
                          children: <Widget>[
                            Container(child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(child: RichText(
                                  text: TextSpan(
                                      text: "Durazno",
                                      style: styles.PoppinsBlack(
                                          fontSize: size.convertLongestSide(context, 16)
                                      )
                                  ),
                                ),),
                                Container(child: RichText(
                                  text: TextSpan(
                                      text: "\$ 100",
                                      style: styles.PoppinsSemiBold(
                                          fontSize: size.convertLongestSide(context, 16)
                                      )
                                  ),
                                ),),
                              ],
                            ),),

                          ],
                        ),),
                      )
                    ],
                  ),
                ),

                SizedBox(height: size.convertWidth(context, 15),),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 15),vertical: size.convertLongestSide(context, 10)),
                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 25)),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.05),
                            blurRadius: 2,
                            spreadRadius: 2,
                            offset: Offset(3,3)
                        )
                      ]
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(child: Image.asset("assets/icons/Durazno.png"),),
                      SizedBox(width: size.convertWidth(context, 12),),
                      Expanded(
                        child: Container(child: Column(
                          children: <Widget>[
                            Container(child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(child: RichText(
                                  text: TextSpan(
                                      text: "Durazno",
                                      style: styles.PoppinsBlack(
                                          fontSize: size.convertLongestSide(context, 16)
                                      )
                                  ),
                                ),),
                                Container(child: RichText(
                                  text: TextSpan(
                                      text: "\$ 100",
                                      style: styles.PoppinsSemiBold(
                                          fontSize: size.convertLongestSide(context, 16)
                                      )
                                  ),
                                ),),
                              ],
                            ),),

                          ],
                        ),),
                      )
                    ],
                  ),
                ),

                SizedBox(height: size.convertWidth(context, 30),),


              ],
            ),
          ),
          ),
          backButton(
            backTap: (){
              Navigator.pop(context);
            },
            istitle: true,
            title: "Orden #28",
          ),
        ],
      ),
    );
  }


}
