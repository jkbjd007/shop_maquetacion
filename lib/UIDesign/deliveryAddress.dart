import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shop_maquetacion/UIDesign/payment.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomBottomAppBar.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/repeatedWigets/radioButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
class deliveryAddress extends StatefulWidget {
  @override
  _deliveryAddressState createState() => _deliveryAddressState();
}

class _deliveryAddressState extends State<deliveryAddress> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(),
    );
  }
  body(){
    return Container(
      child:  Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: size.convertLongestSide(context, 130),),
                Center(child: Image.asset("assets/icons/deliveryBoy.png")),
                SizedBox(height: size.convertLongestSide(context, 70),),

                Container(
                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 53)),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              child: radioButton(
                                selected: true,
                              ),
                            ),
                            SizedBox(width: size.convertWidth(context, 33),),
                            Container(
                              child: RichText(
                                text: TextSpan(
                                    text: "Ubicación actual",
                                    style: styles.PoppinsSemiBold(
                                        fontSize: size.convertLongestSide(context, 17)
                                    )
                                ),
                              ),
                            )

                          ],
                        ),
                      ),
                      SizedBox(height: size.convertLongestSide(context, 20),),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              child: radioButton(
                                selected: true,
                                icon: Icon(IcoFontIcons.plus,
                                color: Colors.white,
                                size: size.convertLongestSide(context, 15),),
                              ),
                            ),
                            SizedBox(width: size.convertWidth(context, 33),),
                            Container(
                              child: RichText(
                                text: TextSpan(
                                    text: "Agregar nueva dirección",
                                    style: styles.PoppinsSemiBold(
                                        fontSize: size.convertLongestSide(context, 17)
                                    )
                                ),
                              ),
                            )

                          ],
                        ),
                      ),
                      SizedBox(height: size.convertLongestSide(context, 20),),
                      Divider(color: appColor,),
                    ],
                  ),
                ),
                SizedBox(height: size.convertLongestSide(context, 25),),
                Container(
                  margin: EdgeInsets.only(left: size.convertWidth(context, 53)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: RichText(
                          text: TextSpan(
                              text: "Direcciones guardadas",
                              style: styles.PoppinsSemiBold(
                                  fontSize: size.convertLongestSide(context, 19)
                              )
                          ),
                        ),
                      ),
                      SizedBox(height: size.convertLongestSide(context, 12),),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              child: radioButton(
                                selected: true,
                              ),
                            ),
                            SizedBox(width: size.convertWidth(context, 33),),
                            Container(
                              child: RichText(
                                text: TextSpan(
                                    text: "Agrarismo #63 col Escandón",
                                    style: styles.PoppinsSemiBold(
                                        fontSize: size.convertLongestSide(context, 17)
                                    )
                                ),
                              ),
                            )

                          ],
                        ),
                      ),
                      SizedBox(height: size.convertLongestSide(context, 12),),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              child: radioButton(
                                selected: false,
                              ),
                            ),
                            SizedBox(width: size.convertWidth(context, 33),),
                            Container(
                              child: RichText(
                                text: TextSpan(
                                    text: "Insurgentes sur #134 ",
                                    style: styles.PoppinsSemiBold(
                                        fontSize: size.convertLongestSide(context, 17)
                                    )
                                ),
                              ),
                            )

                          ],
                        ),
                      ),
                      SizedBox(height: size.convertLongestSide(context, 12),),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              child: radioButton(
                                selected: false,
                              ),
                            ),
                            SizedBox(width: size.convertWidth(context, 33),),
                            Container(
                              child: RichText(
                                text: TextSpan(
                                    text: "Puebla 187 col Roma",
                                    style: styles.PoppinsSemiBold(
                                        fontSize: size.convertLongestSide(context, 17)
                                    )
                                ),
                              ),
                            )

                          ],
                        ),
                      ),

                    ],
                  ),
                ),
                SizedBox(height: size.convertLongestSide(context, 85),),

              ],
            ),
          ),
          backButton(
            backTap: (){
              Navigator.pop(context);
            },
            shadowColor: Colors.black.withOpacity(0.1),
            istitle: true,
            title: "¿A donde te llevamos\n tú pedido?",

          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: size.convertLongestSide(context, 20)),
            alignment: Alignment.bottomCenter,
            height: MediaQuery.of(context).size.height,
            child: filledButton(
              onTap: (){
                Navigator.push(context, PageTransition(
                  child: payment(),
                  type: PageTransitionType.leftToRight
                ));
              },
              leftWidget: Container(
                padding: EdgeInsets.only(left: size.convertWidth(context, 30)),
                child: Text("continuar",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize:  size.convertLongestSide(context, 17) ,
                    fontFamily:  "Poppins-Medium" ,
                  ),
                ),
              ),
              rigtWidget: Container(
                padding: EdgeInsets.only(right: size.convertWidth(context, 30)),
                child: Expanded(
                  child: Text("\$200.00",
                    maxLines: 1,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize:  size.convertLongestSide(context, 17),
                      fontFamily:  "Poppins-Medium" ,
                    ),
                  ),
                ),
              ),
            ),
          ),

        ],
      ),

    );
  }
}
