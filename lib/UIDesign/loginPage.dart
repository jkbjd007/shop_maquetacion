import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shop_maquetacion/UIDesign/forgotPassword.dart';
import 'package:shop_maquetacion/UIDesign/registerationPage.dart';
import 'package:shop_maquetacion/UIDesign/selectCity.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomTextField.dart';
import 'package:shop_maquetacion/repeatedWigets/backButton.dart';
import 'package:shop_maquetacion/repeatedWigets/filledButton.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:icofont_flutter/icofont_flutter.dart';

class loginPage extends StatefulWidget {
  @override
  _loginPageState createState() => _loginPageState();
}

class _loginPageState extends State<loginPage> {
  bool hidePass = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: body());
  }
  body(){
    return Container(
      child: Stack(
        children: <Widget>[
          Container(child:SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: size.convertLongestSide(context, 70),),
                Center(child: Image.asset("assets/icons/greenLogo.png")),
                SizedBox(height: size.convertLongestSide(context, 17),),
                Center(child: RichText(
                  text: TextSpan(
                    text: "¿No tienes cuenta?",
                    style: styles.PoppinsMedium(
                      color: BlackFont
                    )
                  ),
                )),

                SizedBox(height: size.convertLongestSide(context, 17),),
                Center(child: filledButton(
                    onTap: (){
                      print("Navigate to Registeration");
                      Navigator.push(context, PageTransition(
                          child: registerationPage(),
                          type: PageTransitionType.leftToRight
                      ));},
                  txt: "Regístrate",
                )),

                SizedBox(height: size.convertLongestSide(context, 25),),
                Center(child: RichText(
                  text: TextSpan(
                      text: " Iniciar sesión ",
                      style: styles.PoppinsMedium(
                          color: BlackFont
                      )
                  ),
                )),

                SizedBox(height: size.convertLongestSide(context, 20),),
                Center(child: CustomTextField(
                  hints: "Email",
                  iconWidget: Image.asset("assets/icons/email.png"),
                )),

                SizedBox(height: size.convertLongestSide(context, 20),),
                Center(child: GestureDetector(
                  onTap: (){
                    setState(() {
                      hidePass = !hidePass;
                    });
                  },
                  child: CustomTextField(
                    obscureText: hidePass,
                    hints: "Contraseña",
                    iconWidget: Image.asset("assets/icons/lock.png"),
                    trailingIcon: hidePass ? Icon(IcoFontIcons.eyeBlocked,size: size.convertLongestSide(context, 35),) : Icon(IcoFontIcons.eye,size: size.convertLongestSide(context, 35),) ,

                  ),
                )),

                SizedBox(height: size.convertLongestSide(context, 20),),
                Center(child: filledButton(
                  onTap: (){
                    Navigator.push(context, PageTransition(child: selectCity(),
                    type: PageTransitionType.leftToRight));
                  },
                  txt: "Continuar",
                )),

                SizedBox(height: size.convertLongestSide(context, 10),),
                Center(child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, PageTransition(
                      child: forgotPassword(),
                      type: PageTransitionType.leftToRight
                    ));
                  },
                  child: RichText(
                    text: TextSpan(
                        text: "¿Olvidaste tu contraseña? ",
                        style: styles.PoppinsMedium(
                            color: BlackFont
                        )
                    ),
                  ),
                )),

                SizedBox(height: size.convertLongestSide(context, 10),),
                Center(child: Image.asset("assets/icons/fruits.png")),

                SizedBox(height: size.convertLongestSide(context, 10),),

              ],
            ),
          ),
          ),
          backButton(),
        ],
      ),
    );
  }
}
