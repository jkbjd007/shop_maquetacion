import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';

import 'package:flutter/material.dart';
class radioButton extends StatefulWidget {
  Function onchange;
  Widget icon;
  bool selected = true;
  radioButton({this.onchange,this.selected,this.icon});
  @override
  _radioButtonState createState() => _radioButtonState();
}

class _radioButtonState extends State<radioButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: size.convertLongestSide(context, 26),
      height: size.convertLongestSide(context, 26),
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
              width: 1,
              color: widget.selected ? appColor :appColor
          )
      ),
      child: widget.selected ? Container(
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: appColor,
//            border: Border.all(
//                width: 4,
//                color: Colors.white
//            )
        ),
        child: Center(child: widget.icon?? Image.asset("assets/icons/tick.png"),),
      ) : Container(width: 0.1,height: 0.1,),
    );
  }
}
