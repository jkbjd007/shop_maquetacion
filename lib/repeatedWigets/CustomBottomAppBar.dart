import 'package:shop_maquetacion/UIDesign/Favorities.dart';
import 'package:shop_maquetacion/UIDesign/homePage.dart';
import 'package:shop_maquetacion/UIDesign/orderHistory.dart';
import 'package:shop_maquetacion/repeatedWigets/CustomDrawer.dart';
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:page_transition/page_transition.dart';

//import 'package:page_transition/page_transition.dart';
class CustomBottomBar extends StatefulWidget {
  int select;
  GlobalKey<ScaffoldState> scaffoldkey;
  CustomBottomBar({this.select,this.scaffoldkey});

  @override
  _CustomBottomBarState createState() => _CustomBottomBarState();
}

class _CustomBottomBarState extends State<CustomBottomBar> {
  @override
  Widget build(BuildContext context) {
    Color unSelectedColor = Color(0xff7e7e7e);
    Color SelectedColor = appColor;
    TextStyle unselectTextstyle = TextStyle(
      fontFamily: "Lato",
      fontSize: 12,
      color: unSelectedColor,
    );
    TextStyle selectTextstyle = TextStyle(
      fontFamily: "Lato",
      fontSize: 12,
      color: SelectedColor,
    );

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [Color(0xff185729),Color(0xff0B912F)
          ]
        )
      ),
      height: size.convertLongestSide(context, 70),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              setState(() {
                widget.select = 1;
                print("index print  ${widget.select}");
              });
              if(widget.select==1 ){
                widget.scaffoldkey.currentState.openDrawer();
                openDrawer();
//                Navigator.pushReplacement(context,
//                    PageTransition(
//                        type: PageTransitionType.fade, child: medicalRecord()));
              }
            },
            child: Container(
              child: widget.select == 1
                  ? Image.asset(
                      "assets/icons/profile1.png",
                      color: SelectedColor,
                    )
                  : Image.asset("assets/icons/profile1.png"),
            ),
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                widget.select = 2;
                print("index print  ${widget.select}");
              });
              if(widget.select==2){
                Navigator.pushReplacement(context,
                    PageTransition(
                        type: PageTransitionType.fade, child: homePage()));
              }
            },
            child: Container(
              child: widget.select == 2
                  ? Image.asset(
                "assets/icons/home.png",
                color: SelectedColor,
              )
                  : Image.asset("assets/icons/home.png"),
            ),
          ),
          Container(),
          GestureDetector(
            onTap: () {
              setState(() {
                widget.select = 3;
                print("index print  ${widget.select}");
              });
              if(widget.select==3 ){
                Navigator.pushReplacement(context,
                    PageTransition(
                        type: PageTransitionType.fade, child: orderHistory()));
              }
            },
            child: Container(
              child: widget.select == 3
                  ? Image.asset(
                "assets/icons/notification.png",
                color: SelectedColor,
              )
                  : Image.asset("assets/icons/notification.png"),
            ),
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                widget.select = 4;
                print("index print  ${widget.select}");
              });
              if(widget.select==4 ){
                Navigator.pushReplacement(context,
                    PageTransition(
                        type: PageTransitionType.fade, child: Favorities()));
              }
            },
            child: Container(
              child: widget.select == 4
                  ? Image.asset(
                "assets/icons/heart.png",
                color: SelectedColor,
              )
                  : Image.asset("assets/icons/heart.png"),
            ),
          ),
        ],
      ),
    );
  }
  openDrawer() {
    return Drawer(
      child: CustomDrawer(),
    );
  }
}
