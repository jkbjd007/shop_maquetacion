
import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:shop_maquetacion/res/string.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:page_transition/page_transition.dart';


//import 'package:page_transition/page_transition.dart';
//import 'package:percent_indicator/percent_indicator.dart';


class CustomDrawer extends StatefulWidget {
  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  int selectedIndex = 0;
  Color selectedIndexColor = appColor.withOpacity(0.3);

  @override
  Widget build(BuildContext context) {

    print("drawer call");
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
          color: IconsColor
      ),

      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(height: size.convertLongestSide(context, 60),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
                child: Image.asset("assets/icons/logoW.png")),
            SizedBox(height: size.convertLongestSide(context, 50),),
            Container(
                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
                child: RichText(
                  text: TextSpan(
                    text: "Bienvenido",
                    style: styles.PoppinsMedium(
                      fontSize: size.convertLongestSide(context, 17),
                      color: Colors.white
                    )
                  ),
                ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
              child: RichText(
                text: TextSpan(
                    text: "Rodolfo Romero",
                    style: styles.PoppinsSemiBold(
                        fontSize: size.convertLongestSide(context, 17),
                        color: Colors.white
                    )
                ),
              ),
            ),
            SizedBox(height: size.convertLongestSide(context, 280),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
              child: Row(
                children: <Widget>[
                  Image.asset("assets/icons/setting.png"),
                  SizedBox(width: size.convertWidth(context, 10),),
                  Container(
                    child: RichText(
                      text: TextSpan(
                          text: "Configuraciones",
                          style: styles.PoppinsRegular(
                              fontSize: size.convertLongestSide(context, 15),
                              color: Colors.white
                          )
                      ),
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(height: size.convertLongestSide(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
              child: Row(
                children: <Widget>[
                  Image.asset("assets/icons/sesion.png"),
                  SizedBox(width: size.convertWidth(context, 10),),
                  Container(
                    child: RichText(
                      text: TextSpan(
                          text: "Cerrar sesión",
                          style: styles.PoppinsRegular(
                              fontSize: size.convertLongestSide(context, 15),
                              color: Colors.white
                          )
                      ),
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),
      ),

    );
  }
}
