import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class CustomTextField extends StatelessWidget {
  Color color1;
  Color backGroundColor;
  Color hintsColor1;
  Widget iconWidget;
  Function onchange;
  Function ontap;
  Function onsaved;
  Function onEditingComplete;
  Function onSumbit;
  String hints;
  Widget trailingIcon ;
  double borderwidth;
  double height1;
  double width1;
  TextInputType textInputType;
  TextEditingController textEditingController;
  bool obscureText;
  bool isEnable;
  double fontSize;
  FocusNode focusNode;
  bool focusMode;
  CustomTextField({this.fontSize,this.isEnable=true,this.obscureText,this.textInputType,this.borderwidth,this.trailingIcon,this.hints,this.color1, this.iconWidget,this.textEditingController,this.height1,this.width1,this.backGroundColor,this.hintsColor1,this.onchange,this.ontap,this.onsaved,this.onEditingComplete,this.focusNode,this.onSumbit,this.focusMode=false});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width1?? size.convertWidth(context, 356),
      height: height1??size.convertLongestSide(context, 48),
      padding: EdgeInsets.only(right: 15, left: 15),
      decoration: BoxDecoration(
        color: backGroundColor?? Colors.white,
        border: Border.all(
          color: color1 == null ? appColor : color1,
          width: borderwidth==null ? 1 : borderwidth ,
        ),
        borderRadius: BorderRadius.circular(5)
      ),
      child: Row(
        children: <Widget>[
           iconWidget == null ? Container() : iconWidget,
          !focusMode ? SizedBox(width: 22,) : SizedBox(width: 10,),
          Expanded(
            child: !focusMode ? TextFormField(
              onFieldSubmitted: onSumbit?? (val){

              },
              enabled: isEnable,
              obscureText: obscureText ?? false,
              keyboardType: textInputType == null  ? TextInputType.text: textInputType,
              controller: textEditingController,
              style: TextStyle(
                  fontSize: fontSize?? size.convertLongestSide(context, 15),
                  fontFamily: "Poppins-Medium",
                  color: BlackFont
              ),
              decoration: InputDecoration(
                  disabledBorder: InputBorder.none,
                  border: InputBorder.none,
                  hintText: hints==null ? "": hints,
                  hintStyle: TextStyle(
                    fontSize:  size.convertLongestSide(context, 15),
                    fontFamily: "Poppins-Medium",
                    color: hintsColor1?? hintsColor.withOpacity(0.6),
                  )
              ),
              //autofocus: true,
//              style: TextStyle(
//
//              ),

            ) : TextFormField(
              onFieldSubmitted: onSumbit?? (val){

              },
              focusNode: focusNode,
              onEditingComplete: onEditingComplete ?? (){},
              onTap: ontap??(){},
              onSaved: onsaved??(val){},
              enabled: isEnable,
              obscureText: obscureText ?? false,
              keyboardType: textInputType == null  ? TextInputType.text: textInputType,
              controller: textEditingController,
              style: TextStyle(
                fontSize: fontSize?? size.convertLongestSide(context, 15),
                fontFamily: "Poppins-Medium",
                color: BlackFont
              ),
              decoration: InputDecoration(
                disabledBorder: InputBorder.none,
                border: InputBorder.none,
                hintText: hints==null ? "": hints,
                hintStyle: TextStyle(
                  fontSize:  size.convertLongestSide(context, 15),
                  fontFamily: "Poppins-Medium",
                  color: hintsColor1?? hintsColor.withOpacity(0.6),
                )
              ),
              //autofocus: true,
//              style: TextStyle(
//
//              ),

            ),
          ),
          trailingIcon == null ? Container() : trailingIcon,
        ],
      ),
    );
  }
}
