import 'package:shop_maquetacion/res/color.dart';
import 'package:shop_maquetacion/res/size.dart';
import 'package:shop_maquetacion/res/style.dart';
import 'package:flutter/material.dart';
class backButton extends StatelessWidget {
  Function backTap;
  Function likeTap;
  Color shadowColor;
  String title;
  Widget trallingIcon;
  bool isTrallingIcon;
  bool isLeadingIcon;
  bool istitle;
  backButton({this.title,this.trallingIcon,this.isTrallingIcon = false,this.istitle=false,this.isLeadingIcon=true,this.backTap,this.shadowColor,this.likeTap});
  @override
  Widget build(BuildContext context) {
    return Container(

        padding: EdgeInsets.only(left: size.convertWidth(context, 24),
            right: size.convertWidth(context, 24),
            top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          isLeadingIcon ? GestureDetector(
            onTap: backTap??(){
              print("NO back flow");
            },
            child: Container(
              width: size.convertLongestSide(context, 51),
              height: size.convertLongestSide(context, 51),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      //color: Colors.green,
                        color: Colors.black.withOpacity(0.05),
                        blurRadius: 5.0,
                        spreadRadius: 2
                      //offset: new Offset(20.0, 20.0),
                    ),
                  ]
              ),
              child: Center(
                child: Icon(Icons.arrow_back_ios,
                  color: appColor,
                  size: size.convertLongestSide(context, 35),),
              ),
            ),
          ) : Container(width: size.convertLongestSide(context, 51),
    height: size.convertLongestSide(context, 51),
    color: Colors.transparent,),
          istitle ? Expanded(
            child: Container(
              //color: Colors.amberAccent,
    height: size.convertLongestSide(context, 51),
              child: Center(
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                    text: title??"",
                    style: styles.PoppinsSemiBold(
                        color: BlackFont
                    )
                ),
              ),
            ),),
          ) : Container(width: size.convertLongestSide(context, 51),
            height: size.convertLongestSide(context, 51),
            color: Colors.transparent,),
          isTrallingIcon ? GestureDetector(
            onTap: likeTap??(){
              print("NO back flow");
            },
            child: Container(
              width: size.convertLongestSide(context, 51),
              height: size.convertLongestSide(context, 51),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      //color: Colors.green,
                        color: Colors.black.withOpacity(0.05),
                        blurRadius: 5.0,
                        spreadRadius: 2
                      //offset: new Offset(20.0, 20.0),
                    ),
                  ]
              ),
              child: Center(
                child: trallingIcon,
              ),
            ),
          ) : Container(),
        ],
      ),
    );
  }
}
